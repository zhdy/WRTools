package com.wrexagear.tools;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.browser.customtabs.CustomTabsIntent;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.widget.PullRefreshLayout;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    //因listview使用不熟练，所以使用简单的布局方法
    /*变量*/
    String[] ExaGearPkgName = Config.ExaGearPkgName;
    int ExaGearList = 0 ;   //列表项目数量
    List<String> ListData=new ArrayList<String>();
    public static boolean isActive; //是否在前台

    /*组件*/
    private CardView ExaGear1,ExaGear2,ExaGear3,ExaGear4,ExaGear5;
    private ImageView ExaGear1Icon,ExaGear2Icon,ExaGear3Icon,ExaGear4Icon,ExaGear5Icon;
    private TextView ExaGear1Name,ExaGear2Name,ExaGear3Name,ExaGear4Name,ExaGear5Name;
    private TextView ExaGear1Vision,ExaGear2Vision,ExaGear3Vision,ExaGear4Vision,ExaGear5Vision;
    private TextView ExaGear1Sate,ExaGear2Sate,ExaGear3Sate,ExaGear4Sate,ExaGear5Sate;
    private Button UnstallExaGear1,UnstallExaGear2,UnstallExaGear3,UnstallExaGear4,UnstallExaGear5;
    private Button RunExaGear1,RunExaGear2,RunExaGear3,RunExaGear4,RunExaGear5;
    private Toolbar toolbar;
    private TextView TipText;
    private PullRefreshLayout pullRefreshLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    /*初始化*/
    public void init(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        SharedPreferences sharedPreferences = getSharedPreferences("Settings", Context.MODE_PRIVATE);
        //用户协议
        if (!sharedPreferences.getString("userRead", "").equals("over1")){
            AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
            dialog.setIcon(R.drawable.icon);
            dialog.setTitle("ExaGear管理器用户协议");
            dialog.setCancelable(false);
            dialog.setMessage("尊敬的用户您好，欢迎使用ExaGear管理器，为了给您提供更好的服务，请您认真阅读以下协议：\n一、本协内容的生效、变更：\n1.本协议从您点击“我接受”按钮时开始生效，若您不同意本协议，请立即停止使用本软件！\n" +
                    "2.本协议会随着App的更新而修改、变更，当您安装了新版本App时，若协议有变更，将会再次出现本窗口。\n二、软件版权申明：\n1.本软件（“ExaGear管理器”）为开源软件，您可以自由修改本软件并发布其他版本，相关源代码请前往App关于页面。"+
                    "但为了维护我们的劳动成果，请务必在您的修改版本中加入相关开源申明，且保留本协议。\n2.本软件提供下载的模拟器（“ExaGear”）是收集自互联网的第三方破解、修改版本，仅用于学习交流，请勿用于商业用途！\n3.若用户想支持此模拟器，请点击“购买正版”按钮。\n4.用户可以以非商业用途自由"+
                    "传播本软件所储存在其设备的副本。\n三、免责申明：\n本软件已通过用户协议的形式警告用户对于非ExaGear官方资源的使用范围、传播形式等，若用户以商业用途传播“ExaGear”模拟器的相关副本，发生的一切纠纷与本软件以及本软件开发者无关。\n四、隐私条款：\n1.本软件不会收集用户的"+
                    "隐私信息，但为了帮助用户快速定位本软件出现的异常错误，本软件会在崩溃时从用户设备匿名上传错误日志、设备储存空间使用情况、AndroidSdk等级、Root权限状态、设备型号、运行堆栈、CPU架构、系统ROM信息等。\n五、第三方链接："+
                    "\n1.本软件会使用您的浏览器跳转第三方链接，链接内容由第三方提供，与本软件无关。\n2.若本软件引用的链接已失效、出现重大更改或涉及非法内容，请务必向维护者反馈！\n      再次感谢您的使用\n      祝你使用愉快！！\n                                            溟血星空\n                                         2018.11.12");
            dialog.setPositiveButton("我接受", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    SharedPreferences sharedPreferences = getSharedPreferences("Settings", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();//获取编辑器
                    editor.putString("userRead","over1");//储存
                    editor.commit();//提交修改
                }
            });
            dialog.setNegativeButton("我拒绝", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            dialog.setNeutralButton("购买正版", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    OpenUrl("https://eltechs.com");
                    finish();
                }
            });
            dialog.show();
        }
        //保存默认数据
        if (sharedPreferences.getString("WebData", "").equals("")){
            SharedPreferences.Editor editor = sharedPreferences.edit();//获取编辑器
            editor.putString("WebData","官方高速");//储存
            editor.commit();//提交修改
            Toast.makeText(MainActivity.this,"加载默认镜像源完毕",Toast.LENGTH_SHORT).show();
        }
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /**
                 * 检查安装权限
                 * */
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && !getPackageManager().canRequestPackageInstalls()) {
                    Toast.makeText(MainActivity.this, "没有安装权限", Toast.LENGTH_SHORT).show();
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setIcon(R.drawable.icon);
                    builder.setMessage("从Android8.0开始，使用26及以上SDK的应用安装apk时需要独立申请未知来源安装权限，为了您的使用体验，强烈建议您前往授权！或忽略此信息等待安装应用时逐个授权。\n即使获得安装应用的权限，本软件安装应用时仍需您的确认！");
                    builder.setPositiveButton("前往授权", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES);
                            startActivityForResult(intent, 2);
                        }
                    });
                    builder.setNeutralButton("忽略", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            installExaGear();
                        }
                    });
                    builder.show();
                } else {
                    installExaGear();
                }
            }
        });
        fab.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final EditText et = new EditText(MainActivity.this);
                new AlertDialog.Builder(MainActivity.this).setTitle("请输入VCOF脚本地址")
                        .setIcon(R.drawable.icon)
                        .setView(et)
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (et.getText().toString().startsWith("http")||et.getText().toString().startsWith("https://")){
                                    Intent intent =new Intent();
                                    intent.setClass(MainActivity.this,InstallActivity.class);
                                    Config.getUrl = et.getText().toString();
                                    startActivity(intent);
                                }else {
                                    Toast.makeText(MainActivity.this,"内容无效",Toast.LENGTH_LONG).show();
                                }
                            }
                        }).setNegativeButton("取消",null).show();
                return true;
            }
        });
        //下拉刷新
        pullRefreshLayout = (PullRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pullRefreshLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        pullRefreshLayout.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this,"刷新完毕，共找到"+ getnewExaGearList()+"个ExaGear模拟器",Toast.LENGTH_SHORT).show();
                            }
                        });
                        pullRefreshLayout.setRefreshing(false);
                    }
                }, 1000);
            }
        });
        //设置副标题
        toolbar.setSubtitle("正在扫描…");
    //初始化ExaGear'列表'
        //ExaGear列表容器
        ExaGear1 = (CardView) findViewById(R.id.ExaGear1);
        ExaGear2 = (CardView) findViewById(R.id.ExaGear2);
        ExaGear3 = (CardView) findViewById(R.id.ExaGear3);
        ExaGear4 = (CardView) findViewById(R.id.ExaGear4);
        ExaGear5 = (CardView) findViewById(R.id.ExaGear5);
        //ExaGear列表各大组件
        //图标
        ExaGear1Icon = (ImageView) findViewById(R.id.ExaGear1Icon);
        ExaGear2Icon = (ImageView) findViewById(R.id.ExaGear2Icon);
        ExaGear3Icon = (ImageView) findViewById(R.id.ExaGear3Icon);
        ExaGear4Icon = (ImageView) findViewById(R.id.ExaGear4Icon);
        ExaGear5Icon = (ImageView) findViewById(R.id.ExaGear5Icon);
        //应用名
        ExaGear1Name = (TextView) findViewById(R.id.ExaGear1Name);
        ExaGear2Name = (TextView) findViewById(R.id.ExaGear2Name);
        ExaGear3Name = (TextView) findViewById(R.id.ExaGear3Name);
        ExaGear4Name = (TextView) findViewById(R.id.ExaGear4Name);
        ExaGear5Name = (TextView) findViewById(R.id.ExaGear5Name);
        //版本
        ExaGear1Vision = (TextView) findViewById(R.id.ExaGear1Vision);
        ExaGear2Vision = (TextView) findViewById(R.id.ExaGear2Vision);
        ExaGear3Vision = (TextView) findViewById(R.id.ExaGear3Vision);
        ExaGear4Vision = (TextView) findViewById(R.id.ExaGear4Vision);
        ExaGear5Vision = (TextView) findViewById(R.id.ExaGear5Vision);
        //状态
        ExaGear1Sate = (TextView) findViewById(R.id.ExaGear1Sate);
        ExaGear2Sate = (TextView) findViewById(R.id.ExaGear2Sate);
        ExaGear3Sate = (TextView) findViewById(R.id.ExaGear3Sate);
        ExaGear4Sate =  (TextView)findViewById(R.id.ExaGear4Sate);
        ExaGear5Sate =  (TextView)findViewById(R.id.ExaGear5Sate);
        //卸载按钮
        UnstallExaGear1 =  (Button)findViewById(R.id.UnstallExaGear1);
        UnstallExaGear1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UnButtonOnClick(1);
            }
        });
        UnstallExaGear2 = (Button) findViewById(R.id.UnstallExaGear2);
        UnstallExaGear2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UnButtonOnClick(2);
            }
        });
        UnstallExaGear3 = (Button) findViewById(R.id.UnstallExaGear3);
        UnstallExaGear3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UnButtonOnClick(3);
            }
        });
        UnstallExaGear4 = (Button) findViewById(R.id.UnstallExaGear4);
        UnstallExaGear4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UnButtonOnClick(4);
            }
        });
        UnstallExaGear5 = (Button) findViewById(R.id.UnstallExaGear5);
        UnstallExaGear5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UnButtonOnClick(5);
            }
        });
        //启动按钮
        RunExaGear1 = (Button) findViewById(R.id.RunExaGear1);
        RunExaGear1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RunButtonOnClick(1);
            }
        });
        RunExaGear2 = (Button) findViewById(R.id.RunExaGear2);
        RunExaGear2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RunButtonOnClick(2);
            }
        });
        RunExaGear3 = (Button) findViewById(R.id.RunExaGear3);
        RunExaGear3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RunButtonOnClick(3);
            }
        });
        RunExaGear4 = (Button) findViewById(R.id.RunExaGear4);
        RunExaGear4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RunButtonOnClick(4);
            }
        });
        RunExaGear5 = (Button) findViewById(R.id.RunExaGear5);
        RunExaGear5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RunButtonOnClick(5);
            }
        });
        //提示文本
        TipText = findViewById(R.id.TipText);
        //循环获取已安装的ExaGear，并添加到列表
        getnewExaGearList();

        /**
         * 动态申请储存权限
         * */
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
                //没有权限则申请权限
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setIcon(R.drawable.icon);
                builder.setCancelable(false);//设置不可取消
                builder.setTitle("权限申请：");
                builder.setMessage("ExaGear管理器需要储存权限用于储存下载的模拟器数据包、安装包");
                builder.setPositiveButton("我知道了", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},1);
                    }
                });

                builder.show();
            }
        }
    }

    public void installExaGear(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("安装ExaGear");
        builder.setPositiveButton("这些模拟器有什么不同？", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(MainActivity.this,webActivity.class);
                Bundle bundle = new Bundle();
                bundle.putCharSequence("title","这些模拟器有什么不同？");
                bundle.putBoolean("autoTitile",false);
                bundle.putBoolean("visibilityUrl",false);
                bundle.putCharSequence("breakUrl","http://www.wrexagear.com/AppHtml/WET/");
                bundle.putCharSequence("Url","http://www.wrexagear.com/AppHtml/WET/page12.htm");
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        builder.setNegativeButton ("取消", null);
        final String[] item = {"ExaGear_ET", "ExaGear_ED","ExaGear_ED301", "ExaGear_Crv5", "ExaGear_EX"};
        builder.setItems(item, new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                //要安装的ExaGear：item[which]
                SharedPreferences sharedPreferences = getSharedPreferences("Settings", Context.MODE_PRIVATE);
                if (sharedPreferences.getString("WebData", "").equals("")){
                    SharedPreferences.Editor editor = sharedPreferences.edit();//获取编辑器
                    editor.putString("WebData","Gitee");//储存
                    editor.commit();//提交修改
                    Toast.makeText(MainActivity.this,"加载默认镜像源完毕",Toast.LENGTH_SHORT).show();
                }
                //镜像源参数
                String WebDataSh = sharedPreferences.getString("WebData", "");
                if (Config.getDownUrl(WebDataSh) != ""){
                    Intent intent =new Intent();
                    intent.setClass(MainActivity.this,InstallActivity.class);
                    startActivity(intent);
                    Config.getUrl = Config.getDownUrl(WebDataSh)+item[which]+".vcof";
                }else{
                    Toast.makeText(MainActivity.this,"数据异常",Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case 1:
                if (grantResults.length>0&&grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(MainActivity.this,"感谢您的信任",Toast.LENGTH_SHORT).show();
                    //申请权限成功
                }else{
                    //申请失败，再次申请
                    if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
                        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
                            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                            builder.setIcon(R.drawable.icon);
                            builder.setCancelable(false);//设置不可取消
                            builder.setTitle("权限申请：");
                            builder.setMessage("拒绝储存权限可能会引起软件运行异常，建议立即授权，或跳转设置后返回继续使用。");
                            builder.setPositiveButton("我知道了", new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialog, int which)
                                {
                                    openAppSetting("com.wrexagear.tools");
                                }
                            });
                            builder.show();
                        }
                    }
                }
                break;
        }
    }
    @Override
    protected void onResume() {
        if (!isActive) {
            //app 从后台唤醒，进入前台
            isActive = true;
            getnewExaGearList();
        }
        //Activity切换
        getnewExaGearList();
        super.onResume();
    }
    @Override
    protected void onStop() {
        if (!isAppOnForeground()) {
            //app 进入后台
            isActive = false;//记录当前已经进入后台
        }
        super.onStop();
    }

    /*初始化菜单*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /*菜单被选择*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_setting:
                startActivity(new Intent().setClass(MainActivity.this,Setting.class));
                return true;
            case R.id.action_about:
                startActivity(new Intent().setClass(MainActivity.this,aboutActivity.class));
                return true;
            case R.id.action_question:
                Intent intent = new Intent(MainActivity.this,webActivity.class);
                Bundle bundle = new Bundle();
                bundle.putCharSequence("title","常见问题");
                bundle.putBoolean("autoTitile",false);
                bundle.putBoolean("visibilityUrl",false);
                bundle.putCharSequence("breakUrl","http://www.wrexagear.com/AppHtml/WET/");
                bundle.putCharSequence("Url","http://www.wrexagear.com/AppHtml/WET/");
                intent.putExtras(bundle);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * APP是否处于前台唤醒状态
     *
     * @return
     */
    public boolean isAppOnForeground() {
        ActivityManager activityManager = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
        String packageName = getApplicationContext().getPackageName();
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager
                .getRunningAppProcesses();
        if (appProcesses == null)
            return false;

        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            // The name of the process that this object is associated with.
            if (appProcess.processName.equals(packageName)
                    && appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                return true;
            }
        }
        return false;
    }

    public int getnewExaGearList(){
        //初始化列表值
        ExaGearList = 0 ;
        //初始化列表
        ExaGear1.setVisibility(View.GONE);
        ExaGear2.setVisibility(View.GONE);
        ExaGear3.setVisibility(View.GONE);
        ExaGear4.setVisibility(View.GONE);
        ExaGear5.setVisibility(View.GONE);
        //初始化索引
        ListData=new ArrayList<String>();
        //开始处理
        //循环获取已安装的ExaGear，并添加到列表
        int i = 0;
        while ( i < ExaGearPkgName.length){
            //根据包名取应用名，不为空字符则已安装
            if(getApplicationNameByPackageName(ExaGearPkgName[i]) != ""){
                //Toast.makeText(this,"您安装了"+getApplicationNameByPackageName(ExaGearPkgName[i]),Toast.LENGTH_SHORT).show();
                if(ExaGearList == 0 ){
                    ExaGearList = 1;
                }else{
                    ExaGearList ++ ;
                }
                ListData.add(ExaGearPkgName[i]);
                setExaGearIcon(ExaGearList,ExaGearPkgName[i]);
                setExaGearName(ExaGearList,getApplicationNameByPackageName(ExaGearPkgName[i]));
                setExaGearVision(ExaGearList,"版本："+getAppVersionNameByPackageName(MainActivity.this,ExaGearPkgName[i]));
                if (fileIsExists(getSDPath()+"/Android/data/"+ExaGearPkgName[i]+"/"+"files/image/usr/bin/wine")){
                    setExaGearSate(ExaGearList,"状态：正常");
                }else if (ExaGearPkgName[i].equals(Config.ExaGearPkgName[2])){
                    setExaGearSate(ExaGearList,"状态：未知");
                }
                else{
                    setExaGearSate(ExaGearList,"状态：未安装数据包");
                }
                setExaGearList(ExaGearList);
            }
            i ++;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) { //如果Android版本大于7.1则创建Shortcut
            ShortcutManager mShortcutManager;
            mShortcutManager = getSystemService(ShortcutManager.class);
            List<ShortcutInfo> Sc = new ArrayList<ShortcutInfo>();
            int iw = 0;
            while ( iw < ExaGearList & Sc.toArray().length < 5){
                System.out.println("Exa："+ ListData.get(iw));
                if ( ListData.get(iw)!= "" & getApplicationNameByPackageName(ListData.get(iw)) != "" & isActive){
                    Sc.add(
                            new ShortcutInfo.Builder(this,getApplicationNameByPackageName(ListData.get(iw)))
                                    .setShortLabel(getApplicationNameByPackageName(ListData.get(iw)))
                                    .setLongLabel(getApplicationNameByPackageName(ListData.get(iw)))
                                    .setIcon(Icon.createWithResource(this,R.drawable.exagear))
                                    .setIntent(getPackageManager().getLaunchIntentForPackage(ListData.get(iw)))
                                    .build()
                    );
                    System.out.println("循环"+iw);
                }
                iw ++;
                if (Sc != null){
                    mShortcutManager.setDynamicShortcuts(Sc);
                }
            }
        }

        if (ExaGear1.getVisibility() == View.GONE){
            TipText.setText("—没有找到ExaGear，点击悬浮按钮安装—");
        }else{
            TipText.setText("—完毕—");
            //子线程延迟隐藏文字
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(500);
                        TipText.post(new Runnable() {
                            @Override
                            public void run() {
                                TipText.setVisibility(View.GONE);
                            }
                        });
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
        toolbar.setSubtitle("共找到"+ ExaGearList+"个已安装的ExaGear模拟器");
        return ExaGearList;
    }

    /**
     * 列表操作—开始
     * */
    public void setExaGearName(int i,String Name){
        switch (i){
            case 1:
                ExaGear1Name.setText(Name);
                break;
            case 2:
                ExaGear2Name.setText(Name);
                break;
            case 3:
                ExaGear3Name.setText(Name);
                break;
            case 4:
                ExaGear4Name.setText(Name);
                break;
            case 5:
                ExaGear5Name.setText(Name);
                break;
        }
    }
    public void setExaGearIcon(int i,String Name){
        switch (i){
            case 1:
                ExaGear1Icon.setImageDrawable(getApplicationIconByPackageName(MainActivity.this,Name));
                break;
            case 2:
                ExaGear2Icon.setImageDrawable(getApplicationIconByPackageName(MainActivity.this,Name));
                break;
            case 3:
                ExaGear3Icon.setImageDrawable(getApplicationIconByPackageName(MainActivity.this,Name));
                break;
            case 4:
                ExaGear4Icon.setImageDrawable(getApplicationIconByPackageName(MainActivity.this,Name));
                break;
            case 5:
                ExaGear5Icon.setImageDrawable(getApplicationIconByPackageName(MainActivity.this,Name));
                break;
        }
    }
    public void setExaGearVision(int i,String Name){
        switch (i){
            case 1:
                ExaGear1Vision.setText(Name);
                break;
            case 2:
                ExaGear2Vision.setText(Name);
                break;
            case 3:
                ExaGear3Vision.setText(Name);
                break;
            case 4:
                ExaGear4Vision.setText(Name);
                break;
            case 5:
                ExaGear5Vision.setText(Name);
                break;
        }
    }
    public void setExaGearSate(int i,String Name){
        switch (i){
            case 1:
                ExaGear1Sate.setText(Name);
                break;
            case 2:
                ExaGear2Sate.setText(Name);
                break;
            case 3:
                ExaGear3Sate.setText(Name);
                break;
            case 4:
                ExaGear4Sate.setText(Name);
                break;
            case 5:
                ExaGear5Sate.setText(Name);
                break;
        }
    }
    public void setExaGearList(int i){
        switch (i){
            case 1:
                ExaGear1.setVisibility(View.VISIBLE);
                break;
            case 2:
                ExaGear2.setVisibility(View.VISIBLE);
                break;
            case 3:
                ExaGear3.setVisibility(View.VISIBLE);
                break;
            case 4:
                ExaGear4.setVisibility(View.VISIBLE);
                break;
            case 5:
                ExaGear5.setVisibility(View.VISIBLE);
                break;
        }
    }
    /*卸载按钮*/
    public void UnButtonOnClick(int i){
        UninstallApp(ListData.get(i-1));
    }
    /*运行按钮*/
    public void RunButtonOnClick(int i){
        doStartApplicationWithPackageName(ListData.get(i-1));
    }
    /**
     * 列表操作—结束
     * */


    /**
     * 调用类—开始
     * */
    //取应用名
    public String getApplicationNameByPackageName(String packageName) {
        PackageManager pm = getPackageManager();
        String Name ;
        try {
            Name=pm.getApplicationLabel(pm.getApplicationInfo(packageName,PackageManager.GET_META_DATA)).toString();
        } catch (PackageManager.NameNotFoundException e) {
            Name = "" ;
        }
        return Name;}
        //取应用图标
    public static synchronized Drawable getApplicationIconByPackageName(Context context,String pkgname) {
        PackageManager packageManager = null;
        ApplicationInfo applicationInfo = null;
        try {
            packageManager = context.getApplicationContext()
                    .getPackageManager();
            applicationInfo = packageManager.getApplicationInfo(pkgname, 0);
        } catch (PackageManager.NameNotFoundException e) {
            applicationInfo = null;
        }
        Drawable d = packageManager.getApplicationIcon(applicationInfo);
        return d;
    }
    //取应用版本
    public static synchronized String getAppVersionNameByPackageName(Context context,String pkgname) {
        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(
                    pkgname, 0);
            return packageInfo.versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    //卸载App
    public boolean UninstallApp(String packageName) {
        Log.d("Pkg:",packageName);
        boolean b = checkApplication(packageName);
        if (b) {
            if (Build.VERSION.SDK_INT == 28){
                openAppSetting(packageName);
            }else {
                Uri packageURI = Uri.parse("package:".concat(packageName));
                Intent intent = new Intent(Intent.ACTION_DELETE);
                intent.setData(packageURI);
                startActivity(intent);
                return true;
            }
        }
        return false;
    }

    //判断应用是否安装
    public boolean checkApplication(String packageName) {
        if (packageName == null || "".equals(packageName)) {
            return false;
        }
        try {
            getPackageManager().getApplicationInfo(packageName,
                    PackageManager.MATCH_UNINSTALLED_PACKAGES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }
        return false;
    }
    //通过包名运行App
    private void doStartApplicationWithPackageName(String packagename) {
        Intent resolveIntent = getPackageManager()
                .getLaunchIntentForPackage(packagename);
                startActivity(resolveIntent);
    }

    //调用浏览器打开url
    public void OpenUrl(String Url){
        //使用Chrome CustomTabs 提升体验
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        builder.setToolbarColor(Color.rgb(47,105,238));//Toolbar颜色
        builder.setShowTitle(true);//显示标题
        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(MainActivity.this, Uri.parse(Url));
    }

    //获取默认储存器路径
    public String getSDPath() {
        File sdDir = null;
        boolean sdCardExist = Environment.getExternalStorageState()
                .equals(android.os.Environment.MEDIA_MOUNTED);
        if (sdCardExist) {
            sdDir = Environment.getExternalStorageDirectory();
        }
        return sdDir.toString();
    }

    //判断文件是否存在
    public boolean fileIsExists(String strFile)
    {
        try
        {
            File f=new File(strFile);
            if(!f.exists())
            {
                return false;
            }
        }
        catch (Exception e)
        {
            return false;
        }
        return true;
    }

    //跳转到应用管理
    public void openAppSetting(String pkg){
        Intent mIntent = new Intent();
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (Build.VERSION.SDK_INT >= 9) {
            mIntent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
            mIntent.setData(Uri.fromParts("package", pkg, null));
        } else if (Build.VERSION.SDK_INT <= 8) {
            mIntent.setAction(Intent.ACTION_VIEW);
            mIntent.setClassName("com.android.settings", "com.android.setting.InstalledAppDetails");
            mIntent.putExtra("com.android.settings.ApplicationPkgName", pkg);
        }
        startActivity(mIntent);
    }
    /**
     * 调用类—结束
     * */
}