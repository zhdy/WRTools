package com.wrexagear.tools;

import android.app.Application;
import android.content.Context;
import android.os.Handler;

import com.tencent.bugly.Bugly;
import com.tencent.bugly.beta.Beta;

import org.xutils.common.Callback;
import org.xutils.http.RequestParams;
import org.xutils.x;

import java.io.File;

public class MyMobileApplication extends Application {
    private static MyMobileApplication instance;
    private Callback.Cancelable cancelable;
    Handler handler;
    @Override
    public void onCreate() {
        super.onCreate();
        Bugly.init(getApplicationContext(),"6d114fb5bf",false);
        Beta.enableNotification = false;
        x.Ext.init(this);
        instance = this;
    }

    // 获取Application
    public static Context getMyApplication() {
        return instance;
    }
    /**
     * i:索引
     * DoanloadUrl：下载链接
     * SaveUrl：保存位置
     * */
    public void setDownload(final int i, String DoanloadUrl, String SaveUrl){
        RequestParams params = new RequestParams(DoanloadUrl);
        params.setSaveFilePath(SaveUrl);
        params.setCancelFast(true);
        //自动为文件命名
        params.setAutoRename(true);
         cancelable = x.http().get(params, new Callback.ProgressCallback<File>() {
            @Override
            public void onSuccess(File result) {
                //下载完成
                switch (i){
                    case 0:
                        InstallActivity.onSuccess(result);
                        break;
                }
            }
            @Override
            public void onError(Throwable ex, boolean isOnCallback) {
                switch (i){
                    case 0:
                        InstallActivity.onError(ex,isOnCallback);
                        break;
                }
            }
            @Override
            public void onCancelled(CancelledException cex) {
                switch (i){
                    case 0:
                        InstallActivity.onCancelled(cex);
                        break;
                }
            }

            @Override
            public void onFinished() {
                switch (i){
                    case 0:
                        InstallActivity.onFinished();
                        break;
                }
            }
            //网络请求之前回调
            @Override
            public void onWaiting() {
                switch (i){
                    case 0:
                        InstallActivity.onWaiting();
                        break;
                }
            }
            //网络请求开始的时候回调
            @Override
            public void onStarted() {
                switch (i){
                    case 0:
                        InstallActivity.onStarted();
                        break;
                }
            }
            @Override
            public void onLoading(long total, long current, boolean isDownloading) {
                //当前进度和文件总大小
                switch (i){
                    case 0:
                        InstallActivity.onLoading(total,current,isDownloading);
                        break;
                }
            }
        });
    }
    public void cancelDownload(){
        if (cancelable != null){
            cancelable.cancel();
        }
        InstallActivity.isDownLoading = false;
    }
}
