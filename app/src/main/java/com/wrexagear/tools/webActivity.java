package com.wrexagear.tools;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.browser.customtabs.CustomTabsIntent;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.baoyz.widget.PullRefreshLayout;

public class webActivity extends AppCompatActivity {
    private ProgressBar progressBar;
    private WebView webView;
    private PullRefreshLayout pullRefreshLayout;
    private ActionBar actionBar;
    Boolean autoTitile;
    Boolean visibilityUrl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        //获取Intent
        Intent intent = getIntent();
        //获取Bundle
        Bundle bundle = intent.getExtras();
        //获取参数
        String actionTitle= bundle.getString("title"); //标题
        autoTitile = bundle.getBoolean("autoTitile");//自动切换标题
        visibilityUrl = bundle.getBoolean("visibilityUrl");//显示链接
        String url = bundle.getString("Url");//跳转链接
        String breakUrl = bundle.getString("breakUrl");
        init(actionTitle,url,visibilityUrl,breakUrl);
    }

    public void init(String title, String url, Boolean visibilityUrl, final String breakUrl){
        //进度条
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        //浏览框
        webView = (WebView) findViewById(R.id.webView);
        //下拉刷新
        pullRefreshLayout = (PullRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        pullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                webView.reload();
            }
        });
        //标题栏
        actionBar = getSupportActionBar();
        actionBar.setTitle(title);//设置标题
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        WebSettings webSettings = webView.getSettings();//获取webView设置
        webSettings.setJavaScriptEnabled(true);
        //跳转链接
        webView.loadUrl(url);
        //setWebViewClient
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                pullRefreshLayout.setRefreshing(true);
                progressBar.setVisibility(View.VISIBLE);
                if (breakUrl == null){
                    view.loadUrl(url);
                    return super.shouldOverrideUrlLoading(view, url);
                }else if(url.startsWith(breakUrl)){
                    view.loadUrl(url);
                    return super.shouldOverrideUrlLoading(view, url);
                }else{
                    OpenUrl(url);
                    return true;
                }
            }
        });
        webView.setWebChromeClient(new WebChromeClient(){
            public void onProgressChanged(WebView view, int newProgress) {
                if (newProgress == 100) {
                    //加载完毕进度条消失
                    progressBar.setVisibility(View.GONE);
                    pullRefreshLayout.setRefreshing(false);
                } else {
                    //更新进度
                    progressBar.setProgress(newProgress);
                }
                super.onProgressChanged(view, newProgress);
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    /*
    * 调用类
    * */
    //调用浏览器打开url
    public void OpenUrl(String Url){
        //使用Chrome CustomTabs 提升体验
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        builder.setToolbarColor(Color.rgb(47,105,238));//Toolbar颜色
        builder.setShowTitle(true);//显示标题
        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(webActivity.this, Uri.parse(Url));
    }
}
