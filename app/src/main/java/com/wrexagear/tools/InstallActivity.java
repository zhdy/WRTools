package com.wrexagear.tools;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.OkHttpClient;

public class InstallActivity extends AppCompatActivity {
    /**
     *组件
     * */
    private static MyMobileApplication app; //Application
    private static TextView RunTimeText;
    private static Handler handler = new Handler();
    private static ScrollView scrollView;
    private static ActionBar supportActionBar;
    /**
     * 变量
     * */
    private static Context instance;
    String WebData;
    String AppName;
    static String PkgName;
    String UpVersion;
    String DownVersion;
    String ObbUrl;
    String ObbName;
    String downLoaData;
    String[] pj = new String[2];
    List <String> DownUrlList = new ArrayList<String>();
    List <String> DownExaFileName = new ArrayList<String>();
    List <String> DownFileList = new ArrayList<String>();
    List <String> DownFileName = new ArrayList<String>();
    static List <String> DownLoadList = new ArrayList<String>();
    static boolean isDownLoading = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_install);
        init();
    }

    public void init(){
        app =  (MyMobileApplication)getApplication(); //获取Application
        supportActionBar = getSupportActionBar();
        supportActionBar.setTitle("安装ExaGear");
        supportActionBar.setSubtitle("安装即将开始");
        supportActionBar.setHomeButtonEnabled(true);
        supportActionBar.setDisplayHomeAsUpEnabled(true);
        scrollView = (ScrollView) findViewById(R.id.runTimeScrollView);
        RunTimeText = (TextView) findViewById(R.id.RunTimeText);
        RunTimeText.setText(RunTimeText.getText()+"\nVCOF内核版本：V0.1.1");
        RunTimeText.setText(RunTimeText.getText()+"\n官网：www.wrexagear.com");
        RunTimeText.setText(RunTimeText.getText()+"\n开源地址：gitee.com/MSGXingKong/WRTools");
        RunTimeText.setText(RunTimeText.getText()+"\n");
        RunTimeText.setText(RunTimeText.getText()+"\n—Start");
        RunTimeText.setText(RunTimeText.getText()+"\n");
        RunTimeText.setText(RunTimeText.getText()+"\n正在从镜像源获取数据…");
        instance = this;
        if (Config.getUrl == null || getContext() == null){
            Toast.makeText(InstallActivity.this,"数据异常",Toast.LENGTH_SHORT).show();
            finish();
        }else{
            getUrl(Config.getUrl);
        }
    }

    public static Context getContext() {
        return instance;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if(event.getKeyCode() == KeyEvent.KEYCODE_BACK ) {
            if (!supportActionBar.getSubtitle().equals("安装完毕") && !supportActionBar.getSubtitle().equals("任务结束")){
                AlertDialog.Builder builder = new AlertDialog.Builder(InstallActivity.this);
                builder.setIcon(R.drawable.icon);
                builder.setTitle("警告！");
                builder.setMessage("是否取消安装？");
                builder.setPositiveButton("确认", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        Toast.makeText(getContext(),"正在停止下载…",Toast.LENGTH_LONG).show();
                        app.cancelDownload();
                        Config.Downloading = -1;
                        Config.DownLoadingName = new ArrayList<String>();
                        DownLoadList = new ArrayList<String>();
                        PkgName = "";
                        finish();
                    }
                });
                builder.setNegativeButton("取消",null);
                builder.show();
            }else{
                Config.Downloading = -1;
                Config.DownLoadingName = new ArrayList<String>();
                DownLoadList = new ArrayList<String>();
                PkgName = "";
                finish();
            }
            return true;
        }else {
            return super.dispatchKeyEvent(event);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                if (!supportActionBar.getSubtitle().equals("安装完毕") && !supportActionBar.getSubtitle().equals("任务结束")){
                    AlertDialog.Builder builder = new AlertDialog.Builder(InstallActivity.this);
                    builder.setIcon(R.drawable.icon);
                    builder.setTitle("警告！");
                    builder.setMessage("是否取消安装？");
                    builder.setPositiveButton("确认", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            Toast.makeText(getContext(),"正在停止下载…",Toast.LENGTH_LONG).show();
                            app.cancelDownload();
                            Config.Downloading = -1;
                            Config.DownLoadingName = new ArrayList<String>();
                            DownLoadList = new ArrayList<String>();
                            PkgName = "";
                            finish();
                        }
                    });
                    builder.setNegativeButton("取消",null);
                    builder.show();
                }else{
                    Config.Downloading = -1;
                    Config.DownLoadingName = new ArrayList<String>();
                    DownLoadList = new ArrayList<String>();
                    PkgName = "";
                    finish();

                }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static void RunTime(String text){
        RunTimeText.setText(RunTimeText.getText()+"\n"+text);
        //滚动到底部
        scrollView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                scrollView.post(new Runnable() {
                    public void run() {
                        scrollView.fullScroll(View.FOCUS_DOWN);
                    }
                });
            }
        });
    }

    //获取数据返回
    public void returnGet(String data){
        //RunTimeText.setText(RunTimeText.getText()+"\n"+data);
        WebData = data;
        if (data != "" & data.indexOf("{appname}") != -1){
            RunTime("——————");
            String[] Data = data.split("\n");
            int i = 0;
            while (i < Data.length){
                switch (getInsideString(Data[i],"{","}")){
                    case "appname":
                        AppName = getInsideString(Data[i],"[","]");
                        RunTime("应用名："+AppName);
                        break;
                    case "pj":
                        pj[0] = getInsideString(Data[i],"<",">");
                        pj[1] = getInsideString(Data[i],"[","]");
                        break;
                    case "pkgname":
                        PkgName = getInsideString(Data[i],"[","]");
                        RunTime("包名："+PkgName);
                        break;
                    case "upversion":
                        UpVersion = getInsideString(Data[i],"[","]");
                        RunTime("最高支持的Android Api版本："+UpVersion);
                        break;
                    case "downversion":
                        DownVersion = getInsideString(Data[i],"[","]");
                        RunTime("最低支持的Android Api版本："+DownVersion);
                        break;
                    case "version":
                        downLoaData = getInsideString(Data[i],"[","]");
                        break;
                    case "obb":
                        ObbUrl= getInsideString(Data[i],"[","]");
                        ObbName= getInsideString(Data[i],"<",">");
                        break;
                    case "down":
                        DownUrlList.add(getInsideString(Data[i],"[","]"));
                        DownExaFileName.add(getInsideString(Data[i],"<",">"));
                        break;
                    case "file":
                        DownFileList.add(getInsideString(Data[i],"[","]"));
                        DownFileName.add(getInsideString(Data[i],"<",">"));
                        break;
                    case "echo":
                        RunTime("ECHO："+getInsideString(Data[i],"[","]"));
                        break;
                }
                i++;
            }
            RunTime("——————");
            RunTime("当前设备Android Api版本："+ Build.VERSION.SDK_INT);
            if (Build.VERSION.SDK_INT <= Integer.parseInt(UpVersion) & Build.VERSION.SDK_INT >= Integer.parseInt(DownVersion)){
                RunTime(AppName+"：支持当前系统");
                RunTime("——————");
                if(checkApplication(PkgName)){
                    RunTime(AppName+"：已安装，正在检测资源完整性…");
                    if (fileIsExists(getSDPath() + "/Android/obb/"+PkgName+"/"+ObbName)){
                        RunTime("Obb数据包已存在："+"Android/obb/"+PkgName+"/"+ObbName);
                    }else{
                        RunTime("没有找到obb数据包，已加入下载任务");
                        DownLoadList.add(ObbUrl);
                        Config.DownLoadingName.add("OBB");
                    }
                    if (checkApplication("com.p.j")){
                        RunTime("破解补丁已安装：com.p.j");
                    }
                    if (DownFileList.toArray().length != 0){
                        int fieI = 0;
                        while (fieI < DownFileList.toArray().length){
                            if (fileIsExists(getSDPath()+"/"+DownFileName.get(fieI))){
                                RunTime("已跳过已下载的补丁");
                            }else {
                                DownLoadList.add(DownFileList.get(fieI));
                                Config.DownLoadingName.add("SO");
                                RunTime("加入下载任务"+DownFileList.get(fieI));
                            }
                            fieI ++;
                        }
                    }

                }else{
                    RunTime(AppName+"：未安装，正在准备下载资源…");
                    RunTime(AppName+"——————");
                    if (fileIsExists(getSDPath() + "/Android/obb/"+PkgName+"/"+ObbName) == false){
                        RunTime(AppName+"加入下载任务："+ObbUrl);
                        Config.DownLoadingName.add("OBB");
                        DownLoadList.add(ObbUrl);
                    }

                    int Dint = -1;
                    String[] Da = downLoaData.split("-");
                    if (Da.length == 3){
                        int testApi = Build.VERSION.SDK_INT;
                        if ( testApi <= Integer.parseInt(Da[2]) & testApi > Integer.parseInt(Da[1])+1){
                            Dint = 2;
                        }else if (testApi == Integer.parseInt(Da[1]) | testApi == Integer.parseInt(Da[1]) + 1){
                            Dint = 1;
                        }else if(testApi >= Integer.parseInt(Da[0]) & testApi < Integer.parseInt(Da[1])){
                            Dint = 0;
                        }
                    }
                    if (Dint != -1){
                        String du = DownUrlList.get(Dint);
                        if(fileIsExists(getSDPath()+"/Download/WR/"+PkgName+"/"+DownExaFileName.get(Dint))){
                            RunTime("需要的Apk已下载，正在准备安装");
                            installApk(new File(getSDPath()+"/Download/WR/"+PkgName+"/"+DownExaFileName.get(Dint)));
                        }else{
                            RunTime("加入下载任务："+du);
                            Config.DownLoadingName.add(AppName);
                            DownLoadList.add(du);
                        }
                    }else {
                        RunTime("抱歉，没有找到合适的版本");
                    }
                    if (checkApplication("com.p.j") == false){
                        if (fileIsExists(getSDPath()+"/Download/WR/"+PkgName+"/"+ pj[0])){
                            RunTime("需要的Apk已下载，正在准备安装");
                            installApk(new File(getSDPath()+"/Download/WR/"+PkgName+"/"+pj[0]));
                        }else{
                            RunTime("加入下载任务："+pj[1]);
                            DownLoadList.add(pj[1]);
                            Config.DownLoadingName.add("破解包");
                        }
                    }

                    if (DownFileList.toArray().length != 0){
                        int fieI = 0;
                        while (fieI < DownFileList.toArray().length){
                            if (fileIsExists(getSDPath()+"/"+DownFileName.get(fieI))){
                                RunTime("已跳过已下载的补丁");
                            }else {
                                DownLoadList.add(DownFileList.get(fieI));
                                Config.DownLoadingName.add("SO");
                                RunTime("加入下载任务"+DownFileList.get(fieI));
                            }
                            fieI ++;
                        }
                    }

                    }
            }else{
                RunTime(AppName+"：不支持当前系统"+"\n"+"执行完毕！");
                RunTime("——————");
            }
        }else{
            RunTime("获取数据失败，请检查网络链接或切换镜像源");
        }
        if (DownLoadList.toArray().length ==0){
            RunTime("——————");
            RunTime("没有需要下载的文件");
            RunTime("——————");
            supportActionBar.setSubtitle("任务结束");
        }else {
            RunTime("——————");
            Config.Downloading = 0;
            if (Config.DownLoadingName.get(Config.Downloading).equals("OBB")){
                app.setDownload(0,DownLoadList.get(Config.Downloading),getSDPath()+"/Android/obb/"+PkgName+"/");
            }else if(Config.DownLoadingName.get(Config.Downloading).equals("SO")){
                app.setDownload(0,DownLoadList.get(Config.Downloading),getSDPath()+"/");
            }
            else{
                app.setDownload(0,DownLoadList.get(Config.Downloading),getSDPath()+"/Download/WR/"+PkgName+"/");
            }
        }
    }

    public void getUrl(String url){
        OkHttpClient okHttpClient = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        returnGet("");
                    }
                });
            }
            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final String s = response.body().string();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        returnGet(s);
                    }
                });
            }
        });
    }

    /**
     * 下载回调 -开始
     * */
    public static void onWaiting() {
        //等待中
        supportActionBar.setSubtitle("即将开始下载，当前任务："+Config.Downloading + "总共:"+DownLoadList.toArray().length);
        RunTime(Config.DownLoadingName.get(Config.Downloading)+"：等待中…");
    }
    public static void onStarted() {
        //开始
        supportActionBar.setSubtitle("开始下载，当前任务："+Config.Downloading + "总共:"+DownLoadList.toArray().length);
        RunTime(Config.DownLoadingName.get(Config.Downloading)+"：挂起…");
    }
    public static void onSuccess(File result){
        supportActionBar.setSubtitle("下载完毕，当前任务："+Config.Downloading + "总共:"+DownLoadList.toArray().length);
        RunTime(Config.DownLoadingName.get(Config.Downloading)+"：下载完毕");
        //下载成功，获得File对象
        String Suffix = result.getName().substring(result.getName().lastIndexOf(".")+1);
        switch (Suffix){
            case "apk":
                Toast.makeText(getContext(),"即将开始安装，请手动操作",Toast.LENGTH_LONG).show();
                installApk(result);
                break;
        }
        Config.Downloading ++;
        if (Config.Downloading < DownLoadList.toArray().length){
            if (Config.DownLoadingName.get(Config.Downloading).equals("OBB")){
                app.setDownload(0,DownLoadList.get(Config.Downloading),getSDPath()+"/Android/obb/"+PkgName+"/");
            }else if(Config.DownLoadingName.get(Config.Downloading).equals("SO")){
                app.setDownload(0,DownLoadList.get(Config.Downloading),getSDPath()+"/");
            }else{
                app.setDownload(0,DownLoadList.get(Config.Downloading),getSDPath()+"/Download/WR/"+PkgName+"/");
            }
            //滚动到底部
            scrollView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    scrollView.post(new Runnable() {
                        public void run() {
                            scrollView.fullScroll(View.FOCUS_DOWN);
                        }
                    });
                }
            });
        }else{
            supportActionBar.setSubtitle("安装完毕");
        }
    }
    public static void onError(Throwable ex, boolean isOnCallback){

    }
    public static void onCancelled(org.xutils.common.Callback.CancelledException cex) {
        //下载取消
    }
    public static void onFinished(){
        //下载完毕
    }
    public static void onLoading(long total, long current, boolean isDownloading){
        isDownLoading = isDownloading;
        //下载进度回调
        Double to = Double.valueOf(total);
        Double cu = Double.valueOf(current);
        Double dn = (cu/to)*100;
        if (cu == to){
            RunTime(Config.DownLoadingName.get(Config.Downloading)+"："+current+"/"+total+"("+dn+"%）{断点续传中…}");
        }else{
            RunTime(Config.DownLoadingName.get(Config.Downloading)+"："+current+"/"+total+"("+dn+"%）");
        }
        supportActionBar.setSubtitle("下载中，当前任务："+(Config.Downloading + 1) + "总共："+DownLoadList.toArray().length);
    }
    /**
     * 下载回调 -结束
     * */

    /**
     * 调用类 -开始
     * */
    public  String  getInsideString(String  str, String strStart, String strEnd ) {
        if ( str.indexOf(strStart) < 0 ){
            return "";
        }
        if ( str.indexOf(strEnd) < 0 ){
            return "";
        }
        return str.substring(str.indexOf(strStart) + strStart.length(), str.indexOf(strEnd));
    }
    public boolean checkApplication(String packageName) {
        if (packageName == null || "".equals(packageName)) {
            return false;
        }
        try {
            getPackageManager().getApplicationInfo(packageName,
                    PackageManager.MATCH_UNINSTALLED_PACKAGES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }
        return false;
    }
    //获取默认储存器路径
    public static String getSDPath() {
        File sdDir = null;
        boolean sdCardExist = Environment.getExternalStorageState()
                .equals(android.os.Environment.MEDIA_MOUNTED);
        if (sdCardExist) {
            sdDir = Environment.getExternalStorageDirectory();
        }
        return sdDir.toString();
    }

    //判断文件是否存在
    public boolean fileIsExists(String strFile)
    {
        try
        {
            File f=new File(strFile);
            if(!f.exists())
            {
                return false;
            }
        }
        catch (Exception e)
        {
            return false;
        }
        return true;
    }
    public static void installApk(File file) {
        if (file == null){
            Toast.makeText(getContext(),"抱歉，安装出错",Toast.LENGTH_SHORT).show();
            return;
        }
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Log.d("installApk", "版本大于 N ，开始使用 fileProvider 进行安装");
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            Uri contentUri = FileProvider.getUriForFile(getContext(), "com.wrexagear.tools.fileProvider", file);
            intent.setDataAndType(contentUri, "application/vnd.android.package-archive");
        } else {
            Log.d("installApk", "正常进行安装");
            intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
        }
        getContext().startActivity(intent);
    }
}