package com.wrexagear.tools;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.net.Uri;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.tencent.bugly.beta.Beta;

import java.util.List;

public class aboutActivity extends AppCompatActivity {
    private ActionBar actionBar;
    private TextView appVision;
    private TextView appVisionCode;
    private CardView getThanks;
    private CardView getUpData;
    private CardView lookCode;
    private CardView openCode;
    private CardView appWeb;
    private CardView friendLink;
    private CardView goCoolApk;
    private CardView getWeb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        init();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void init(){
        //初始化标题
        actionBar = getSupportActionBar();
        actionBar.setTitle("关于");
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        //应用名
        appVision = (TextView) findViewById(R.id.appVision);
        appVisionCode = (TextView) findViewById(R.id.appVisionCode);
        try {
            PackageManager pkgManager = this.getPackageManager();
            PackageInfo pkgInfo = pkgManager.getPackageInfo(this.getPackageName(), 0);
            appVision.setText("版本："+ pkgInfo.versionName);
            appVisionCode.setText("版本号："+ pkgInfo.versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        //鸣谢
        getThanks = (CardView) findViewById(R.id.getThanks);
        getThanks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(aboutActivity.this);
                builder.setIcon(R.drawable.icon);
                builder.setTitle("鸣谢");
                builder.setMessage(
                                "hostei\n"+
                                "恶魔程序员\n"+
                                "初心\n" +
                                "凋零葉\n"+
                                "墨意花间\n"+
                                "℃_C\n"+
                                "黑の剑姬\n"+
                                "\n————ExaGear管理器，感谢有你（以上排名不分先后）"
                );
                builder.setNegativeButton("关闭",null);
                builder.show();
            }
        });
        //检查更新
        getUpData = (CardView) findViewById(R.id.getUpData);
        getUpData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Beta.checkUpgrade();
            }
        });
        //查看源代码
        lookCode = (CardView) findViewById(R.id.lookCode);
        lookCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenUrl("https://gitee.com/MSGXingKong/WRTools");
            }
        });
        //开放源代码许可
        openCode = (CardView) findViewById(R.id.openCode);
        openCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(aboutActivity.this);
                builder.setIcon(R.drawable.icon);
                builder.setTitle("开源相关");
                builder.setMessage(
                        "okhttp3:\nOkHttp is an HTTP client that’s efficient by default:"+
                                "\n\nhttp://square.github.io/okhttp/"+
                                "\n——————————"+
                                "\nxutils：\nxUtils 包含了orm, http(s), image, view注解, 但依然很轻量级(246K), 并且特性强大, 方便扩展"+
                                "\n\nhttps://github.com/wyouflf/xUtils"+
                                "\n——————————"+
                                "\nChrome Custom Tabs:\nChrome Custom Tabs provides a way for an application to customize and interact with a Chrome Activity on Android. This makes the web content feel like being a part of the application, while retaining the full functionality and performance of a complete web browser."+
                                "\n\nhttps://github.com/GoogleChrome/custom-tabs-client"+
                                "\n——————————"+
                                "\nbugly：\n一种愉悦的开发方式 _android anr_android anr分析_iOS崩溃日志分析平台"+
                                "\n\nhttps://bugly.qq.com"+
                                "\n——————————"+
                                "\npullrefreshlayout：\nThis component like SwipeRefreshLayout, it is more beautiful than SwipeRefreshLayout."+
                                "\n\nhttps://github.com/baoyongzhang/android-PullRefreshLayout"+
                                "\n——————————"+
                                "\nVCOF："+
                                "\n由蔚然团队_溟血星空瞎搞的高效率标记语言"+
                                "\n\nhttps://gitee.com/MSGXingKong/WRVCF"
                );
                builder.setNegativeButton("关闭",null);
                builder.show();
            }
        });
        appWeb = (CardView) findViewById(R.id.appWeb);
        appWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenUrl("http://www.wrexagear.com");
            }
        });
        goCoolApk = (CardView) findViewById(R.id.goCoolApk);
        goCoolApk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(aboutActivity.this,"如果ExaGear管理器帮助到了你，记得好评哦…",Toast.LENGTH_LONG).show();
                OpenUrl("https://www.coolapk.com/apk/206154");
            }
        });
        getWeb = (CardView) findViewById(R.id.getWeb);
        getWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenUrl("https://xui.ptlogin2.qq.com/cgi-bin/xlogin?f_url=loginerroralert&hide_title_bar=1&style=35&appid=1000101&s_url=https://support.qq.com/product/40638/");
            }
        });
        friendLink = (CardView) findViewById(R.id.friendLink);
        friendLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(aboutActivity.this);
                builder.setTitle("友情链接");
                builder.setNegativeButton ("取消", null);
                final String[] item = {"阿帝云-帝者德合天地曰帝","52模拟-ExaGear模拟器交流站"};
                builder.setItems(item, new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        switch (item[which]){
                            case "阿帝云-帝者德合天地曰帝":
                                OpenUrl("https://www.adiyun.com");
                                break;
                            case "52模拟-ExaGear模拟器交流站":
                                OpenUrl("http://52emu.cn");
                        }
                    }
                });
                builder.setPositiveButton("我也要加友链", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String[] reciver = new String[]{"mingxuexk@gmail.com"};
                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setType("message/rfc822"); // 设置邮件格式
                        intent.putExtra(Intent.EXTRA_EMAIL, reciver); // 接收人
                        intent.putExtra(Intent.EXTRA_SUBJECT, "ExaGear管理器友链申请"); // 主题
                        intent.putExtra(Intent.EXTRA_TEXT, "网站名称：xxxxx\n网站链接：http(s)://www.xxx.xx\n站长邮箱：xxxxxx@xx.com\n站长QQ：\n网站介绍："); // 正文
                        PackageManager packageManager = getPackageManager();
                        List<ResolveInfo> activities = packageManager.queryIntentActivities(intent, 0);
                        boolean isIntentSafe = activities.size() > 0;
                        if(isIntentSafe)
                            startActivity(intent);
                        else
                            Toast.makeText(aboutActivity.this, "装个邮件邮箱软件，如何？",Toast.LENGTH_SHORT).show();
                    }
                });
                builder.show();
            }
        });
    }
    /**
     * 调用类
     * */
    //调用浏览器打开url
    public void OpenUrl(String Url){
        //使用Chrome CustomTabs 提升体验
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        builder.setToolbarColor(Color.rgb(47,105,238));//Toolbar颜色
        builder.setShowTitle(true);//显示标题
        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(aboutActivity.this, Uri.parse(Url));
    }
}