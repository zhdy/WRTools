package com.wrexagear.tools;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class Setting extends AppCompatActivity {
    String[] WebData = Config.WebData;
    //组件
    private TextView WebDataText;
    private TextView WebSateText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        init();
    }

    public void init(){
        ActionBar supportActionBar = getSupportActionBar();
        supportActionBar.setTitle("设置");
        supportActionBar.setHomeButtonEnabled(true);
        supportActionBar.setDisplayHomeAsUpEnabled(true);

        //读取设置
        WebDataText = (TextView) findViewById(R.id.settingWeb_item);
        SharedPreferences sharedPreferences = getSharedPreferences("Settings", Context.MODE_PRIVATE);
        //getString()第二个参数为缺省值，如果preference中不存在该key，将返回缺省值
        String WebDataSh = sharedPreferences.getString("WebData", "");
        if (WebDataSh != ""){

        }else{
            //获取editor对象
            SharedPreferences.Editor editor = sharedPreferences.edit();//获取编辑器
            editor.putString("WebData",WebData[0]);//储存
            editor.commit();//提交修改
            WebDataSh = sharedPreferences.getString("WebData", "");
        }
        WebDataText.setText(WebDataSh);

        //获取网络设置item
        final CardView SettingWeb = (CardView) findViewById(R.id.SettingWeb);
        SettingWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Setting.this);
                builder.setTitle("选择数据源");
                builder.setPositiveButton("提供镜像数据源？", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                    }
                });
                builder.setNegativeButton ("取消", null);
                builder.setItems(WebData, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        //获取sharedPreferences对象
                        SharedPreferences sharedPreferences = getSharedPreferences("Settings", Context.MODE_PRIVATE);
                        //获取editor对象
                        SharedPreferences.Editor editor = sharedPreferences.edit();//获取编辑器
                        editor.putString("WebData",WebData[which]);//储存
                        editor.commit();//提交修改
                        String WebDataSh = sharedPreferences.getString("WebData", "");
                        WebDataText.setText(WebDataSh);
                    }
                });
                builder.show();
            }
        });
        //获取网络检测
        WebSateText = (TextView) findViewById(R.id.WebSateTextSub);
        if (Config.WebSate != null & Config.WebSate != ""){
            WebSateText.setText(Config.WebSate);
        }
        final CardView WebSate = (CardView) findViewById(R.id.WebSate);
        WebSate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Setting.this,webSateActivity.class);
                startActivity(intent);
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
         case android.R.id.home:
                 onBackPressed();
             return true;
    }
    return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        if (Config.WebSate != null & Config.WebSate != ""){
            WebSateText.setText(Config.WebSate);
        }
        super.onResume();
    }

    /**
     * 调用类
     * */
    //调用浏览器打开url
    public void OpenUrl(String Url) {
        //使用Chrome CustomTabs 提升体验
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        builder.setToolbarColor(Color.rgb(47, 105, 238));
        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(Setting.this, Uri.parse(Url));
    }
}