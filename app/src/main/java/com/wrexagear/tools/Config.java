package com.wrexagear.tools;

import java.util.ArrayList;
import java.util.List;

public class Config {
    /**
     * 全局变量
     * */
    static String getUrl;
    static int Downloading;
    static List<String> DownLoadingName = new ArrayList<String>();
    static String WebSate = "未检测";
    /**
     * 配置数据
     * */
    static String[] ExaGearPkgName = new String[]{ //ExaGear包名数组
            "com.eltechs.et",   //ExaGearET版模拟器
            "com.eltechs.es",   //ExaGearCrv5(ES)版模拟器
            "com.eltechs.ed",   //ExaGearED版模拟器
            "com.eltechs.ex"    //ExaGearEX版模拟器
    };
    static String[] WebData = new String[]{ //下载数据源
            "官方高速",
            "Gitee（备用）"  //开源中国
    };
    static String[] WebDataUrl = new String[]{
            "http://114.116.81.12/VCOF/WET/ExaGear/",
            "https://gitee.com/MSGXingKong/WRVCF/raw/master/WET/ExaGear/"
    };

    /**
     * 匹配数据
     * */
    public static String getDownUrl(String name){
        switch (name){
            case "官方高速":
                return WebDataUrl[0];
            case "Gitee（备用）":
                return WebDataUrl[1];
        }
        return "";
    }
}
