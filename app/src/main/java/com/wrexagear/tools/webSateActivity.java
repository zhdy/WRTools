package com.wrexagear.tools;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.IOException;

public class webSateActivity extends AppCompatActivity {
    //组件
    private ActionBar actionBar;
    private TextView WebRunTimeText;
    private ScrollView scrollView;
    private static Handler handler = new Handler();

    //变量
    String[] runUrl = new String[]{"http://www.wrexagear.com","https://www.adiyun.com","https://www.gitee.com","http://114.116.81.12","https://onedrive.gimhoy.com"};
    String[] runUrlName = new String[]{"ExaGear管理器官网","adiyun备用下载节点","gitee备用数据节点","蔚然数据中心高速节点","gimhoy高速下载节点"};
    int runIngInt = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_sate);
        init();
    }
    public void init(){
        //设置标题
        actionBar = getSupportActionBar();
        actionBar.setTitle("网络检测");
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        //获取运行时文字
        WebRunTimeText = (TextView) findViewById(R.id.WebRunTimeText);
        //获取scrollView
        scrollView = (ScrollView) findViewById(R.id.WebRunTime);
        if (runIngInt < runUrl.length){
            RunTime("开始检测："+runUrlName[runIngInt]+"("+ runUrl[runIngInt] +")");
            getUrl(runUrl[runIngInt]);
        }
    }

    public void returnGet(String data){
        Boolean isOk;
        if (data.equals("true")){
            RunTime(runUrlName[runIngInt] + "("+ runUrl[runIngInt] +")：正常");
        }else{
            RunTime(runUrlName[runIngInt] + "("+ runUrl[runIngInt] +")：异常");
        }
        runIngInt ++;
        if (runIngInt < runUrl.length){
            RunTime("开始检测："+runUrlName[runIngInt]+"("+ runUrl[runIngInt] +")");
            getUrl(runUrl[runIngInt]);
        }else{
            Config.WebSate = "检测完毕";
            RunTime("检测完毕");
        }
    }
    public void getUrl(String url){
        OkHttpClient okHttpClient = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        returnGet("false");
                    }
                });
            }
            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final String s = response.body().string();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        returnGet("true");
                    }
                });
            }
        });
    }
    public void RunTime(String text){
        WebRunTimeText.setText(WebRunTimeText.getText()+"\n"+text);
        //滚动到底部
        scrollView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                scrollView.post(new Runnable() {
                    public void run() {
                        scrollView.fullScroll(View.FOCUS_DOWN);
                    }
                });
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
